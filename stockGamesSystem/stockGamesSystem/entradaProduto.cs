﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace stockGamesSystem
{
    public partial class frmEntradaProduto : Form
    {
        Conexao con = new Conexao();
        public frmEntradaProduto()
        {
            InitializeComponent();
        }

        //Essa função pega o nome selecionado no comboBox e procura na tabela produto o ID dele.
        public string PegaID(ComboBox cmb)
        {
            string sqlConsulta, idconsulta;

            sqlConsulta = "select idproduto from produtos where nome=:nome";

            OracleCommand comando = new OracleCommand(sqlConsulta, con.getConnection());

            comando.Parameters.Add(new OracleParameter(":nome", cmb.Text));

            OracleDataReader reader = comando.ExecuteReader();
            reader.Read();

            idconsulta = Convert.ToString(reader.GetInt32(0));

            return idconsulta;
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cmbProduto.Text))
            {
                MessageBox.Show("Consulte o cliente que deseja alterar clicando no Campo Produto", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string sqlQuantidade;

            sqlQuantidade = "update produtos set quantidade=(select quantidade from produtos where idproduto=:FKproduto)+:quant where idproduto=:FKproduto";

            OracleCommand cmdQuant = new OracleCommand(sqlQuantidade, con.getConnection());

            cmdQuant.Parameters.Add(new OracleParameter("=:FKproduto", PegaID(cmbProduto)));
            cmdQuant.Parameters.Add(new OracleParameter("=:quant", txtQuantidade.Text));

            cmdQuant.ExecuteNonQuery();
            MessageBox.Show("Estoque alterado com sucesso", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);

            
            string sqlQuery;

            sqlQuery = "insert into estoque (idestoque,fk_produto,quantidade,preco_de_compra,criado_em) select nvl(max(idestoque),0)+1,:FKprod,:qtde,:prec_compra,CURRENT_TIMESTAMP from estoque";
            
            OracleCommand cmd = new OracleCommand(sqlQuery, con.getConnection());

            cmd.Parameters.Add(new OracleParameter("=:FKprod", PegaID(cmbProduto)));
            cmd.Parameters.Add(new OracleParameter("=:qtde", txtQuantidade.Text));
            cmd.Parameters.Add(new OracleParameter("=:prec_compra", txtValorUnit.Text));

            cmd.ExecuteNonQuery();
            MessageBox.Show("Estoque alterado com sucesso", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void frmEntradaProduto_Load(object sender, EventArgs e) 
        {
            //Esse evento no Form pega os nomes da tabela produto e coloca no comboBox
            DataTable dtTabelas = new DataTable();

            OracleDataAdapter da = new OracleDataAdapter("select * from produtos", con.getConnection());

            da.Fill(dtTabelas);

            cmbProduto.DataSource = dtTabelas;
            cmbProduto.DisplayMember = "nome";
            cmbProduto.ValueMember = "";

            cmbProduto.SelectedIndex = -1;
        }
    }
}
