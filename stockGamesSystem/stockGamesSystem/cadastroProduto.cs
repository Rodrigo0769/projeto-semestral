﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace stockGamesSystem
{
    public partial class frmCadastroProduto : Form
    {
        Conexao con = new Conexao();
        //Essa variável situação vai ser usada para diferenciar as ações do botão salvar

        string situacao;

        TextBox txtNome = new TextBox();

        public frmCadastroProduto()
        {
            InitializeComponent();
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            cmbNome.Visible=false;
            btnEditar.Visible = false;
            btnExcluir.Visible = false;


            txtNome.Location = new Point(116, 125);
            txtNome.Size = new Size(161, 21);

            this.Controls.Add(txtNome);

            situacao = "salvar";
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            switch (situacao)
            {
                case "salvar":
                    {
                        string sqlQuery;

                        sqlQuery = "insert into produtos (idproduto,nome,categoria,classificacao,preco,quantidade,imgcaminho,criado_em) select nvl(max(idproduto),0)+1,:nome,:categoria,:classif,:preco,0,:caminho,current_timestamp from produtos";

                        OracleCommand cmd = new OracleCommand(sqlQuery, con.getConnection());

                        cmd.Parameters.Add(new OracleParameter(":nome", txtNome.Text));
                        cmd.Parameters.Add(new OracleParameter(":categoria", txtCategoria.Text));
                        cmd.Parameters.Add(new OracleParameter(":classif", txtClassificacao.Text));
                        cmd.Parameters.Add(new OracleParameter(":preco", txtPreco.Text));
                        cmd.Parameters.Add(new OracleParameter(":CAMINHO", openFileDialog1.FileName));

                        cmd.ExecuteNonQuery();

                        MessageBox.Show("Produto Cadastrado", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case "editar":
                    {
                        //O evento do Botão editar faltou concluir
                        string sqlQuery;

                        sqlQuery = "UPDATE produtos SET nome = :nome,categoria =:categoria,classificacao =:classif,preco =:preco WHERE idproduto = 1";

                        OracleCommand cmd = new OracleCommand(sqlQuery, con.getConnection());

                        cmd.Parameters.Add(new OracleParameter(":nome", cmbNome.Text));
                        cmd.Parameters.Add(new OracleParameter(":categoria", txtCategoria.Text));
                        cmd.Parameters.Add(new OracleParameter(":classif", txtClassificacao.Text));
                        cmd.Parameters.Add(new OracleParameter(":preco", txtPreco.Text));

                        cmd.ExecuteNonQuery();

                        MessageBox.Show("Produto Editado", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;

            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            btnNovo.Visible = false;
            btnExcluir.Visible = false;

            DataTable dtTabelas = new DataTable();

            OracleDataAdapter da = new OracleDataAdapter("select * from produtos", con.getConnection());

            da.Fill(dtTabelas);

            cmbNome.DataSource = dtTabelas;
            cmbNome.DisplayMember = "nome";
            cmbNome.ValueMember = "";

            cmbNome.SelectedIndex = -1;

            situacao ="editar";
        }

        private void frmCadastroProduto_Load(object sender, EventArgs e)
        {
            //Esse evento no Form pega os nomes da tabela produto e coloca no comboBox
            DataTable dtTabelas = new DataTable();

            OracleDataAdapter da = new OracleDataAdapter("select * from produtos", con.getConnection());

            da.Fill(dtTabelas);

            cmbNome.DataSource = dtTabelas;
            cmbNome.DisplayMember = "nome";
            cmbNome.ValueMember = "";

            cmbNome.SelectedIndex = -1;

        }

        private void cmbNome_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCategoria.Text = cmbNome.Text;
        }

        private void btnAddCapa_Click(object sender, EventArgs e)
        {
            //Esse evento adiciona uma imagem no pictureBox
            try
            {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {

                    picImagem.Load(openFileDialog1.FileName);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
