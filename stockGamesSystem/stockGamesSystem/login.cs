﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

namespace stockGamesSystem
{
    public partial class login : Form
    {

        Conexao con = new Conexao();

        public login()
        {
            InitializeComponent();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {

            string cmdQuery = "SELECT * FROM Funcionarios WHERE NOME=:usuario AND SENHA=:senha";
            OracleCommand cmd = new OracleCommand(cmdQuery, con.getConnection());

            cmd.Parameters.Add(":usuario", txtLogin.Text);
            cmd.Parameters.Add(":senha", txtSenha.Text);
            /*
            string x;
            x=Convert.ToString( txtLogin.Text);
            MessageBox.Show(x);
            */
            /**/
            OracleDataReader reader = cmd.ExecuteReader();


                if (reader.Read())
                {
                    telaPrincipal telaPrcl = new telaPrincipal();
                    telaPrcl.Show();

                    MessageBox.Show("Bem-vindo \n" + txtLogin.Text);
                }
                else
                {
                    MessageBox.Show("Usuario ou Senha incorretos!");
                }
            
        }
    }   
}
