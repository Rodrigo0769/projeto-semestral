﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stockGamesSystem
{
    public partial class telaPrincipal : Form
    {
        public telaPrincipal()
        {
            InitializeComponent();

            Form form = new login();
            form.Close();
            
        }

        private void fornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new cadastroFornecedor();
            form.MdiParent = this;
            form.Show();
        }

        private void funcionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new cadastroFuncionario();
            form.MdiParent = this;
            form.Show();
        }

        private void produtoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new frmCadastroProduto();
            form.MdiParent = this;
            form.Show();
        }

        private void fornecedorToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form form = new pesquisaFornecedor();
            form.MdiParent = this;
            form.Show();
        }

        private void funcionárioToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form form = new pesquisaFuncionario();
            form.MdiParent = this;
            form.Show();
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new pesquisaCliente();
            form.MdiParent = this;
            form.Show();
        }

        private void vendasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new pesquisaVendasRealizadas();
            form.MdiParent = this;
            form.Show();
        }

        private void relVendasToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form form = new relatorioVendas();
            form.MdiParent = this;
            form.Show();
        }

        private void entradaDeProdutosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new frmEntradaProduto();
            form.MdiParent = this;
            form.Show();
        }

        private void entradaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new entradaProdutoNF();
            form.MdiParent = this;
            form.Show();
        }

        private void estoqueMinimoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new estoqueProduto();
            form.MdiParent = this;
            form.Show();
        }

        private void perfisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new cadastraPerfil();
            form.MdiParent = this;
            form.Show();
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new aboutSoftware();
            form.MdiParent = this;
            form.Show();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Close();
            Form frm = new login();
            frm.Show();
        }
    }
}
