﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Oracle.DataAccess.Client;



namespace stockGamesSystem
{
    public partial class cadastroFuncionario : Form
    {
        Conexao con = new Conexao();
        String situacao;
        TextBox txtNome = new TextBox();
        TextBox txtIdFuncionario = new TextBox();

        private void Limparformularios()
        {
            cmbNome.Enabled = false;
            txtSobrenome.Clear();
            txtEndereco.Clear();
            mskAdt.Clear();
            mskDem.Clear();
            txtEmail.Clear();
            txtTelefone.Clear();
            txtTelefone2.Clear();
            txtCelular.Clear();
            txtSenha.Clear();
            cmbCargo.Enabled = false;
            txtSalario.Clear();
        }
        public cadastroFuncionario()
        {
            InitializeComponent();
        }

        // subrotinas para Habilitar controles
        private void Hablitar()
        {
            cmbNome.Enabled = true;
            txtSobrenome.Enabled = true; ;
            txtEndereco.Enabled = true; ;
            txtNumero.Enabled = true; ;
            mskAdt.Enabled = true; ;
            mskDem.Enabled = true; ;
            txtEmail.Enabled = true; ;
            txtTelefone.Enabled = true; ;
            txtTelefone2.Enabled = true; ;
            txtCelular.Enabled = true; ;
            txtSenha.Enabled = true; ;
            cmbCargo.Enabled = true; ;
            txtSalario.Enabled = true; ;
        }

        // subrotinas para Desabilitar controles
        private void Desabilitar()
        {
            cmbNome.Enabled = false;
            txtSobrenome.Enabled = false;
            txtEndereco.Enabled = false;
            txtNumero.Enabled = false;
            mskAdt.Enabled = false;
            mskDem.Enabled = false;
            txtEmail.Enabled = false;
            txtTelefone.Enabled = false;
            txtTelefone2.Enabled = false;
            txtCelular.Enabled = false;
            txtSenha.Enabled = false;
            cmbCargo.Enabled = false;
            txtSalario.Enabled = false;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {


            switch (situacao)
            {
                case "Inserir":
                    {


                        // delcaração da variavel de para qardar intruções em Oracle
                        string OracleQuery = "insert into funcionarios (ID_Func,nome,sobrenome,endereco,numero,DTADM,DTDEM,email,telefone,telefone2,celular,cargo,salario,senha) select nvl(max(Id_func),0)+1,:nome,:sobrenome,:endereco,:numero,:DTADM,:DETDEM,:email,:telefone,:telefone2,:celular,:cargo,:salario,:senha from funcionarios";

                        // cria conexão chamndo metodo getConectionda clase conexao
                        OracleCommand command = new OracleCommand(OracleQuery, con.getConnection());

                        // command.Parameters.Add(new OracleParameter("==:id", txtId.Text));
                        command.Parameters.Add(new OracleParameter("==:nome", txtNome.Text));
                        /*
                        int i = Convert.ToInt32(cmbNome.Text);
                        while (i <= 1000)
                        {
                            cmbNome.Items.Add(i);
                            i++;

                        }
                        */

                        command.Parameters.Add(new OracleParameter("==:sobrenome", txtSobrenome.Text));
                        command.Parameters.Add(new OracleParameter("==:endereco", txtEndereco.Text));
                        command.Parameters.Add(new OracleParameter("==:numero", Convert.ToInt32(txtNumero.Text)));
                        command.Parameters.Add(new OracleParameter("==:DTADM", Convert.ToDateTime(mskAdt.Text)));
                        command.Parameters.Add(new OracleParameter("==:DTDEM", Convert.ToDateTime(mskDem.Text)));
                        command.Parameters.Add(new OracleParameter("==:email", txtEmail.Text));
                        command.Parameters.Add(new OracleParameter("==:telefone", Convert.ToInt32(txtTelefone.Text)));
                        command.Parameters.Add(new OracleParameter("==:telefone2", Convert.ToInt32(txtTelefone2.Text)));
                        command.Parameters.Add(new OracleParameter("==:celular", Convert.ToInt32(txtCelular.Text)));
                        command.Parameters.Add(new OracleParameter("==:cargo", cmbCargo.Text));
                        command.Parameters.Add(new OracleParameter("==:salario", Convert.ToInt32(txtSalario.Text)));
                        command.Parameters.Add(new OracleParameter("==:senha", txtSenha.Text));

                        command.ExecuteNonQuery();

                        MessageBox.Show("Funcionario cadastrado");

                        con.getConnection().Close();
                    }
                    break;

                case "Alterar":
                    {
                        //os campos para serem alterados são preencidos por meio da consulta no grid
                        //veririfica se tem o código do cliente no txtCodigo
                        if (string.IsNullOrEmpty(txtIdFuncionario.Text))
                        {
                            MessageBox.Show("Consulte o cliente que deseja alterar clicando no botão consultar", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }

                        // antes de alterar o registro é preciso validar os dados - TextBox preenchimento obrigatório
                        //chama o médoto para validar a entrada de dados
                        //se retornou falso, interrompe o processamento

                        /* if (Validados() == false)
                          {
                              return;
                          }
                          */

                        //declaração da variável para guardar as instruções SQL
                        string Oraclequery;

                        //cria conexão chamando o método getConnection da classe Conexao
                        OracleConnection alterar = con.getConnection();

                        //cria a instrução sql, parametrizada
                        Oraclequery = "update funcionarios set nome=:nome where Id_func=:alterar";
                        //",sobrenome=:sobrenome,endereco=:endereco,numero=:numero,DTADM=:DTADM,DTDEM=:DETDEM,email=:email,telefone=:telefone,telefone2=:telefone2,celular=:celular,cargo=:cargo,salario=:salario,senha=:senha where id_func=:id";

                        //Tratamento de exceções
                        try
                        {
                            //con.getConnection().Open();
                            OracleCommand command = new OracleCommand(Oraclequery, alterar);

                            //define, adiciona os parametros

                            command.Parameters.Add(new OracleParameter("==:alterar", txtIdFuncionario.Text));
                            command.Parameters.Add(new OracleParameter("==:nome", txtNome.Text));
                            //command.Parameters.Add(new OracleParameter(null, txtSobrenome.Text));
                            //command.Parameters.Add(new OracleParameter(null, txtEndereco.Text));
                            //command.Parameters.Add(new OracleParameter(null, Convert.ToInt32(txtNumero.Text)));
                            //command.Parameters.Add(new OracleParameter(null, Convert.ToDateTime(mskAdt.Text)));
                            //command.Parameters.Add(new OracleParameter(null, Convert.ToDateTime(mskDem.Text)));
                            //command.Parameters.Add(new OracleParameter(null, txtEmail.Text));
                            //command.Parameters.Add(new OracleParameter(null, Convert.ToInt32(txtTelefone.Text)));
                            //command.Parameters.Add(new OracleParameter(null, Convert.ToInt32(txtTelefone2.Text)));
                            //command.Parameters.Add(new OracleParameter(null, Convert.ToInt32(txtCelular.Text)));
                            //command.Parameters.Add(new OracleParameter(null, cmbCargo.Text));
                            //command.Parameters.Add(new OracleParameter(null, Convert.ToInt32(txtSalario.Text)));
                            //command.Parameters.Add(new OracleParameter(null, txtSenha.Text));

                            //executa o commando
                            command.ExecuteNonQuery();

                            MessageBox.Show("Funcionário alterado com sucesso", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //Limpa os campos para nova entrada de dados
                            Limparformularios();
                        }



                        catch (Exception ex)
                        {
                            MessageBox.Show("Problema ao alterar cliente " + ex, "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        finally
                        {
                            if (alterar != null)
                            {
                                alterar.Close();
                            }
                        }
                    }
                    break;
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {

            txtIdFuncionario.Visible = true;
            lblIdentificacao.Visible = true;
            cmbNome.Visible = false;
            btnNovo.Visible = false;
            btnExcluir.Visible = false;

            // txtNome para ser feito o inserssaõ no banco
            txtNome.Location = new Point(173, 116);
            txtNome.Size = new Size(250, 21);
            this.Controls.Add(txtNome);

            // txtIdFuncionario pra aIdentificação do funcionario a ser deletado
            txtIdFuncionario.Location = new Point(254, 87);
            txtIdFuncionario.Size = new Size(88, 20);
            this.Controls.Add(txtIdFuncionario);

            lblIdentificacao.Location = new Point(266, 54);
            lblIdentificacao.Size = new Size(68, 13);
            this.Controls.Add(lblIdentificacao);


            situacao = "Alterar";

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            //veririfica se tem o código do cliente no txtCodigo
            if (string.IsNullOrEmpty(txtIdFuncionario.Text))
            {
                MessageBox.Show("Consulte o cliente que deseja excluir clicando no botão consultar", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;

            }

            //solicita confirmação de exclusão de registro
            if (MessageBox.Show("Deseja excluir permanentemente o registro?", "Stock Games", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //declaração da variável para guardar as instruções SQL
                string Oraclequery;

                //cria conexão chamando o método getConnection da classe Conexao
                OracleConnection conCliente = con.getConnection();

                //cria a instrução sql, parametrizada
                Oraclequery = "DELETE FROM funcionarios WHERE Id_func=:id_func";

                //Tratamento de exceções
                try
                {
                    //conCliente.Open();
                    OracleCommand command = new OracleCommand(Oraclequery, conCliente);
                    //define, adiciona os parametros
                    command.Parameters.Add(new OracleParameter(":id_func", txtIdFuncionario.Text));

                    //executa o commando
                    command.ExecuteNonQuery();

                    MessageBox.Show("Cliente excluído com sucesso", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Limpa os campos para nova entrada de dados
                    // Limparformularios();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Problema ao excluir cliente " + ex, "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                finally
                {
                    if (con.getConnection() != null)
                    {
                        con.getConnection().Close();
                    }
                }

            }
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            cmbNome.Visible = false;
            btnEditar.Visible = false;
            btnExcluir.Visible = false;



            txtNome.Location = new Point(173, 116);
            txtNome.Size = new Size(250, 21);
            this.Controls.Add(txtNome);

            situacao = "Inserir";
        }

        private void cadastroFuncionario_Load(object sender, EventArgs e)
        {

            DataTable dtTable = new DataTable();

            OracleDataAdapter da = new OracleDataAdapter("Select * from funcionarios", con.getConnection());

            da.Fill(dtTable);

            cmbNome.DataSource = dtTable;
            cmbNome.DisplayMember = "name";
            cmbNome.ValueMember = "";

            cmbNome.SelectedIndex = -1;

            // txtIdFuncionario pra aIdentificação do funcionario a ser deletado
            txtIdFuncionario.Visible = true;

            txtIdFuncionario.Location = new Point(254, 87);
            txtIdFuncionario.Size = new Size(88, 20);
            this.Controls.Add(txtIdFuncionario);

            lblIdentificacao.Location = new Point(266, 54);
            lblIdentificacao.Size = new Size(68, 13);
            this.Controls.Add(lblIdentificacao);
        }
    }
}
