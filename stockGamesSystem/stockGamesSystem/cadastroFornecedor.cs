﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

namespace stockGamesSystem
{
    public partial class cadastroFornecedor : Form
    {
        Conexao con = new Conexao();

        public cadastroFornecedor()
        {
            InitializeComponent();
        }







        private void habilitar()
        {


            cmbNome.Enabled = true;
            txtEndereco.Enabled = true;
            txtNLocal.Enabled = true;
            txtEmail.Enabled = true;
            txtTelefone.Enabled = true;
            txtTelefone2.Enabled = true;
            txtCelular.Enabled = true;

        }

        private void desabilitar()
        {
            cmbNome.Enabled = false;
            txtEndereco.Enabled = false;
            txtNLocal.Enabled = false;
            txtEmail.Enabled = false;
            txtTelefone.Enabled = false;
            txtTelefone2.Enabled = false;
            txtCelular.Enabled = false;
        }

        private void limparControles()
        {

            cmbNome.Focus();
            cmbNome.Enabled = false;
            txtEndereco.Clear();
            txtNLocal.Clear();
            txtEmail.Clear();
            txtTelefone.Clear();
            txtTelefone2.Clear();
            txtCelular.Clear();
        }

        private bool validaDados()
        {
            if (string.IsNullOrEmpty(cmbNome.Text))
            {
                MessageBox.Show("Preenchimento de campo obrigatorio", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);


                cmbNome.Focus();

                return false;
            }

            /* if (string.IsNullOrEmpty(txtEmail.Text))
             {
                 MessageBox.Show("Preenchimento de campo obrigatorio", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 txtEmail.Clear();

                 txtEmail.Focus();

                 return false;
             }*/
            return true;

            /*private void frmCadastroFornecedor_Load(object sender, EventArgs e)
                
            {
                habilitar();
            }
            */




        }

        private void btnEditar_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(txtIdFornecedor.Text))
            {
                MessageBox.Show("Consulte o cliente que deseja alterar clicando no botão consultar", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // antes de alterar o registro é preciso validar os dados - TextBox preenchimento obrigatório
            //chama o médoto para validar a entrada de dados
            //se retornou falso, interrompe o processamento
            if (validaDados() == false)
            {
                return;
            }

            //declaração da variável para guardar as instruções SQL
            string oraclequery;

            OracleConnection conAlterar = con.getConnection();

            //cria a instrução sql, parametrizada
            oraclequery = "UPDATE Fornecedor set nome=:nome,Endereco=:Endereco,Nlocal=:Nlocal,Email=:Email,Telefone=:Telefone,Telefone2=:Telefone2,Celular=:Celular where IDFornecedor=:IDFornecedor";

            //Tratamento de exceções
            try
            {

                OracleCommand cmd = new OracleCommand(oraclequery, conAlterar);
                //define, adiciona os parametros
                cmd.Parameters.Add(new OracleParameter(":nome", cmbNome.Text));
                cmd.Parameters.Add(new OracleParameter(":Endereco", txtEndereco.Text));
                cmd.Parameters.Add(new OracleParameter(":NLocal", txtNLocal.Text));
                cmd.Parameters.Add(new OracleParameter(":Email", txtEmail.Text));
                cmd.Parameters.Add(new OracleParameter(":Telefone", txtTelefone.Text));
                cmd.Parameters.Add(new OracleParameter(":Telefone2", txtTelefone2.Text));
                cmd.Parameters.Add(new OracleParameter(":Celular", txtCelular.Text));
                cmd.Parameters.Add(new OracleParameter(":IDFornecedor", Convert.ToInt32(txtIdFornecedor.Text)));

                //executa o commando
                cmd.ExecuteNonQuery();

                MessageBox.Show("Cliente alterado com sucesso", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //Limpa os campos para nova entrada de dados
                limparControles();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema ao alterar Fornecedor" + ex, "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                if (conAlterar != null)
                {
                    conAlterar.Close();
                }
            }



        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            string OracleQuery = "insert into Fornecedor (IDFornecedor,Nome,Endereco,NLocal,Email,Telefone,Telefone2,Celular) Values (2,:Nome,:Endereco,:Nlocal,:Email,:Telefone,:Telefone2,:Celular)";

            OracleCommand command = new OracleCommand(OracleQuery, con.getConnection());

            //command.Parameters.Add(new OracleParameter(":IDFornecedor", txtIdFornecedor.Text));
            command.Parameters.Add(new OracleParameter(":Nome", cmbNome.Text));
            command.Parameters.Add(new OracleParameter(":Endereco", txtEndereco.Text));
            command.Parameters.Add(new OracleParameter(":NLocal", txtNLocal.Text));
            command.Parameters.Add(new OracleParameter(":Email", txtEmail.Text));
            command.Parameters.Add(new OracleParameter(":Telefone", txtTelefone.Text));
            command.Parameters.Add(new OracleParameter(":Telefone2", txtTelefone2.Text));
            command.Parameters.Add(new OracleParameter(":Celular", txtCelular.Text));

            command.ExecuteNonQuery();

            MessageBox.Show("Fornecedor Cadastrado");

            con.getConnection().Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            //verifica se tem o codigo do cliente no txtIDFornecedor
            if (string.IsNullOrEmpty(txtIdFornecedor.Text))
            {
                MessageBox.Show("Consulte o cliente que deseja excluir clicando no botão excluir", "Stock Game", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //Solicita confirmação de exclusão de registro
            if (MessageBox.Show("Deseja excluir permanentemente o registro ?", "Stock Game", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //declaração da variavel para guardar as informações SQL
                string sqlQuery;

                //Criar conexão chamando o metodo getConnection da classe Conexao
                OracleConnection conExcluir = con.getConnection();

                //cria a instrução sql, parametrizada
                sqlQuery = "DELETE FROM FOrnecedor WHERE IDFornecedor = :IDFornecedor";

                try
                {
                    OracleCommand cmd = new OracleCommand(sqlQuery, conExcluir);
                    //define, adiciona os parametros 
                    cmd.Parameters.Add(new OracleParameter(":IDFornecedor", Convert.ToInt32(txtIdFornecedor.Text)));

                    //executa o comando
                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Fornecedor excluido com sucesso", "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Limpar os campos para nova entrada de dados
                    limparControles();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Problema ao excluir Fornecedor" + ex, "Stock Games", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                finally
                {
                    if (conExcluir != null)
                    {
                        conExcluir.Close();
                    }
                }
            }
        }
        /*
        private void btnConsultaFornecedor_Click(object sender, EventArgs e)
        {
            Form frm = new frmpesquisaFornecedor(this);
            frm.MdiParent = this.MdiParent;
            frm.Show();
        }
        */
    }
}
